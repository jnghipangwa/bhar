# Assignment

- Course Title: **Trends in Artificial Intelligence and Machine Learning**
- Course Code: **TAI911S**
- Assessment: First Assignment
- Released on: 09/03/2023
- Due Date: 30/03/2023

# Problem

## Problem Description

Consider the **time series** dataset available at this [link](https://www.kaggle.com/datasets/shenba/time-series-datasets/discussion?select=sales-of-shampoo-over-a-three-ye.csv).
It represents the sales of shampoo over three years. Your task is to train a model using the **Flux** package in the **Julia** language that can forecast sales for
subsequent months. Your model will follow the TPA-LSTM architecture presented in [[1]](#1).

## Assessment Criteria

We will follow the criteria below to assess the problem:

- Data preparation and preprocessing. (15%)
- Model implementation in compliance with TPA-LSTM. (50%)
- Evaluation Metrics. (15%)
- Overall solution in **Julia** and **Flux**. (20%)

# Submission Instructions

- This assignment is to be completed individually.
- For each group, a repository should be created on [Gitlab](https://about.gitlab.com).
- The submission date is Thursday, March 30 2023, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Students who fail to submit on time will be awarded the mark 0.
- Each student is expected to present his/her project after the submission deadline.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.

## References

<a id="1">[1]</a>
Shih, Shun-Yao and Sun, Fan-Keng and Lee, Hung-Yi (2019).
Temporal Pattern Attention for Multivariate Time Series Forecasting.
Kluwer Academic Publishers, 108(8-9), 1421-1441.
